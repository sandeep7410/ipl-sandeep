# no of matches per year
import csv
import matplotlib.pyplot as plt

import sql_queries
# presentation logic


def plot_no_of_matches_per_year(years):
    plt.bar(years.keys(), years.values())
    plt.xlabel('years')
    plt.ylabel('matches')
    plt.title('Number of matches per year')
    plt.show()

# data structure


def calculate_no_of_matches_per_year(matches_file_path):
    years = {}
    with open(matches_file_path) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for row in matches_reader:
            if row["season"] not in years:
                years[row["season"]] = 1
            else:
                years[row["season"]] += 1
    print(years)
    return years


def calculate_and_plot_no_of_matches_per_year(matches_file_path):
    result = calculate_no_of_matches_per_year(matches_file_path)
    plot_no_of_matches_per_year(result)


def sql_query():
    result=sql_queries.no_of_matches_per_year()
    plot_no_of_matches_per_year(result)

if __name__ == '__main__':
    calculate_and_plot_no_of_matches_per_year('./matches.csv')
    sql_query()
