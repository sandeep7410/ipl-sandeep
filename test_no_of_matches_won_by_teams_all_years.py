# Plot a stacked bar chart of matches won of all teams over all the years of IPL.
import unittest

from no_of_matches_won_by_teams_all_years import *


class TestQuery2(unittest.TestCase):

    def test_calculate_no_of_matches_won_by_teams_all_years(self):
        inputs_and_outputs = [
            ('./mock_matches.csv', {'2017': {'Rising Pune Supergiants': 10, 'Kolkata Knight Riders': 9, 'Kings XI Punjab': 7, 'Royal Challengers Bangalore': 3, 'Sunrisers Hyderabad': 7, 'Mumbai Indians': 12, 'Delhi Daredevils': 6, 'Gujarat Lions': 4},
            '2016': {'Kolkata Knight Riders': 8, 'Gujarat Lions': 9, 'Royal Challengers Bangalore': 9, 'Mumbai Indians': 7, 'Delhi Daredevils': 7, 'Kings XI Punjab': 4, 'Sunrisers Hyderabad': 11, 'Rising Pune Supergiants': 4}})
        ]
        for inp, expected_output in inputs_and_outputs:
            output = calculate_no_of_matches_won_by_teams_all_years(inp)
            self.assertEqual(output, expected_output)
        outputs=[{2016: {'Delhi Daredevils': 7, 'Gujarat Lions': 9, 'Kings XI Punjab': 4, 'Kolkata Knight Riders': 8, 'Mumbai Indians': 7, 'Rising Pune Supergiants': 5, 'Royal Challengers Bangalore': 9, 'Sunrisers Hyderabad': 11}, 
        2017: {'Delhi Daredevils': 6, 'Gujarat Lions': 4, 'Kings XI Punjab': 7, 'Kolkata Knight Riders': 9, 'Mumbai Indians': 12, 'Rising Pune Supergiants': 10, 'Royal Challengers Bangalore': 3, 'Sunrisers Hyderabad': 8}}]
        for expected_output in outputs:
            output=sql_query()
            self.assertEqual(output,expected_output)

if __name__ == '__main__':
    unittest.main()