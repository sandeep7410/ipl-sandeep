select count(season) from matches group by season order by season;

select season, winner, count(winner) from mock_matches group by season,winner order by season;

select del.bowling_team,sum(del.extra_runs) from deliveries del inner join matches mat on del.match_id=mat.id where mat.season=2016 group by del.bowling_team;

select del.bowler, cast(sum(del.total_runs)as decimal)/count(total_runs)*6 as economy from deliveries del inner join matches mat on del.match_id=mat.id where mat.season=2015 group by del.bowler order by economy limit 5;


select season,sum(case when toss_winner=winner and toss_decision='bat' then 1 else 0 end) as win,
sum(case when toss_winner!=winner and toss_decision='bat' then 1 else 0 end )as lose,
sum(case when toss_decision='bat' and result='tie' then 1 else 0 end)as tie from matches group by season order by season;
