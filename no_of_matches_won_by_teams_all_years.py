# Plot a stacked bar chart of matches won by all teams over all the years of IPL.

import csv
import matplotlib.pyplot as plt
import sql_queries
# presentation logic


def plot_no_of_matches_won_by_teams_all_years(no_of_wins_by_teams_all_years):
    team_color = {
        'Sunrisers Hyderabad': '#ff0000',
        'Mumbai Indians': '#ffee00',
        'Gujarat Lions': '#8cff00',
        'Rising Pune Supergiants': '#00ff5d',
        'Royal Challengers Bangalore': '#00faff',
        'Kolkata Knight Riders': '#002aff',
        'Delhi Daredevils': '#c700ff',
        'Kings XI Punjab': '#ff008c',
        'Chennai Super Kings': '#ff0010',
        'Rajasthan Royals': '#007fff',
        'Pune Warriors': '#00ff5d',
        'Deccan Chargers': '#1de802',
        'Kochi Tuskers Kerala': '#01e8d0'
    }
    sorted_years = sorted(no_of_wins_by_teams_all_years)
    for year in sorted_years:
        value = no_of_wins_by_teams_all_years[year]
        height = 0
        for team in team_color:
            plt.bar(year, value[team], width=0.5,
                    bottom=height, color=team_color[team])
            height += value[team]

    plt.xlabel('years')
    plt.ylabel('no of wins')
    plt.legend(['SRH', 'MI', 'GL', 'RSPG', 'RCB', 'KKR', 'DD',
                'KXIP', 'CSK', 'RR', 'PW', 'DC', 'KTK'], ncol=5)
    axes = plt.gca()
    axes.set_ylim([0, 100])
    plt.show()


# data structure
def calculate_no_of_matches_won_by_teams_all_years(matches_file_path):
    no_of_years = {}
    teams = []
    with open(matches_file_path) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for match in matches_reader:
            if match['season'] not in no_of_years:
                no_of_years[match['season']] = {}
            else:
                key = no_of_years[match['season']]
                if match['winner'] not in key and len(match['winner']) != 0:
                    teams.append(match['winner'])
                    key[match['winner']] = 1
                elif len(match['winner']) > 0:
                    key[match['winner']] += 1
    for year in no_of_years:
        for specificTeam in teams:
            if specificTeam in no_of_years[year]:
                continue
            else:
                no_of_years[year][specificTeam] = 0
    print(no_of_years)
    return no_of_years


def calculate_and_plot_no_of_matches_won_by_teams_all_years(matches_file_path):
    result = calculate_no_of_matches_won_by_teams_all_years(matches_file_path)
    plot_no_of_matches_won_by_teams_all_years(result)

def sql_query():
    result=sql_queries.no_of_matches_won_by_teams_all_years()
    plot_no_of_matches_won_by_teams_all_years(result)

if __name__ == '__main__':
    calculate_and_plot_no_of_matches_won_by_teams_all_years('./matches.csv')
    sql_query()