import csv
def get_match_id(matches_file_path,queryYear):
    idList = []
    with open(matches_file_path) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for row in matches_reader:
            if row['season'] == queryYear:
                idList.append(row['id'])
    return idList
