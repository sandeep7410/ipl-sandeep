# For the year 2015 plot the top economical bowlers.
import unittest

from top_economical_bowlers import *


class TestQuery4(unittest.TestCase):

    def test_calculate_top_economical_bowlers(self):
        inputs_and_outputs = [
            (['./mock_deliveries.csv','./mock_matches.csv'], 
{'M Morkel': 2.0, 'I Sharma': 4.0, 'NM Coulter-Nile': 4.0, 'AD Russell': 8.5, 'MJ McClenaghan': 14.0})
        ]
        for inp, expected_output in inputs_and_outputs:
            output = calculate_top_economical_bowlers(*inp)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()