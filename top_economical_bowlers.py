# For the year 2015 plot the top economical bowlers.
import csv
import matplotlib.pyplot as plt
import utils
import operator
import sql_queries


def calculate_and_plot_top_economical_bowlers(deliveries_file_path, matches_file_path):
    result = calculate_top_economical_bowlers(
        deliveries_file_path, matches_file_path)
    plot_top_economical_bowlers(result)


def plot_top_economical_bowlers(top_economical_bowlers):
    plt.bar(top_economical_bowlers.keys(),
            top_economical_bowlers.values(), width=0.7)
    plt.title('Top 5 economical bowlers')
    plt.show()


def calculate_top_economical_bowlers(deliveries_file_path, matches_file_path):
    queryYear = '2015'
    match_id_list = utils.get_match_id(matches_file_path, queryYear)
    bowlers_economy = {}
    with open(deliveries_file_path) as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        for row in deliveries_reader:
            if row['match_id'] not in match_id_list:
                continue
            else:
                specific_bowler = row['bowler']
                if specific_bowler in bowlers_economy:
                    if row['extra_runs'] == '0':
                        bowlers_economy[specific_bowler]['noOfBalls'] += 1
                    bowlers_economy[specific_bowler]['totalRuns'] += int(
                        row['total_runs'])
                else:
                    bowlers_economy[specific_bowler] = {
                        'noOfBalls': 1,
                        'totalRuns': int(row['total_runs'])
                    }
                    if row['extra_runs'] != '0':
                        bowlers_economy[specific_bowler]['noOfBalls'] = 0
    for bowler in bowlers_economy:
        key = bowlers_economy[bowler]
        bowlers_economy[bowler] = (key['totalRuns']/key['noOfBalls'])*6
    bowlers_economy = sorted(bowlers_economy.items(),
                             key=operator.itemgetter(1))
    bowlers_economy = dict(bowlers_economy)
    top_economical_bowlers = {k: bowlers_economy[k] for k in list(bowlers_economy)[
        :5]}
    print(top_economical_bowlers)
    return top_economical_bowlers


def sql_query():
    result=sql_queries.top_economical_bowlers()
    plot_top_economical_bowlers(result)


if __name__ == '__main__':
    calculate_and_plot_top_economical_bowlers('./deliveries.csv','./matches.csv')
    sql_query()
