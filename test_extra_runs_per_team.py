# For the year 2016 plot the extra runs conceded per team.
import unittest

from extra_runs_per_team import *


class TestQuery3(unittest.TestCase):

    def test_calculate_extra_runs_per_team(self):
        inputs_and_outputs = [
            (['./mock_deliveries.csv','./mock_matches.csv'], 
{'Rising Pune Supergiants': 11, 'Mumbai Indians': 3, 'Kolkata Knight Riders': 1, 'Delhi Daredevils': 0})
        ]
        for inp, expected_output in inputs_and_outputs:
            output = calculate_extra_runs_per_team(*inp)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()