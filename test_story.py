# probability of a team winning the match if it wins the toss and choose to bat in every year
import unittest

from story import *


class TestQuery4(unittest.TestCase):

    def test_calculate(self):
        inputs_and_outputs = [
            (['./mock_matches.csv'], 
{'2017': {'win': 6, 'tie': 1, 'lose': 4}, '2016': {'win': 2, 'tie': 0, 'lose': 9}})
        ]
        for inp, expected_output in inputs_and_outputs:
            output = calculate(*inp)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()