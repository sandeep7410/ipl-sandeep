# all the queries for this project
import psycopg2


def connection():
    return psycopg2.connect(database='ipl', user='postgres',
                            password='Nsandeep', host='127.0.0.1', port='5432')


def no_of_matches_per_year():
    con=connection()
    cur = con.cursor()
    cur.execute(
        'select season,count(season) from matches group by season order by season;')
    noompy = cur.fetchall()
    print(noompy)
    con.close()
    return dict(noompy)


def no_of_matches_won_by_teams_all_years():
    con=connection()
    cur = con.cursor()
    cur.execute(
        'select season, winner, count(winner) from matches where winner is not null group by season,winner order by season;')
    noomwbtay = cur.fetchall()
    con.close()
    teams = []
    d = {}
    for row in noomwbtay:
        if row[1] not in teams:
            teams.append(row[1])
        if row[0] in d:
            d[row[0]][row[1]] = row[2]
        else:
            d[row[0]] = {}
            d[row[0]][row[1]] = row[2]
    for year in d:
        for specificTeam in teams:
            if specificTeam not in d[year]:
                d[year][specificTeam] = 0
    print(d)
    return d


def story():
    con=connection()
    cur = con.cursor()
    cur.execute('''select season,sum(case when toss_winner=winner and toss_decision='bat' then 1 else 0 end) as win,
sum(case when toss_winner!=winner and toss_decision='bat' then 1 else 0 end )as lose,
sum(case when toss_decision='bat' and result='tie' then 1 else 0 end)as tie from matches group by season order by season;''')
    str = cur.fetchall()
    con.close()
    d = {}
    for record in str:
        d[record[0]] = {'win': record[1], 'lose': record[2], 'tie': record[3]}
    return d


def top_economical_bowlers():
    con=connection()
    cur = con.cursor()
    cur.execute('''select del.bowler, cast(sum(del.total_runs)as decimal)/sum(case when extra_runs=0 then 1 else 0 end)*6 as economy 
    from deliveries del inner join matches mat on del.match_id=mat.id where mat.season=2015
    group by del.bowler order by economy limit 5;''')
    teb = cur.fetchall()
    con.close()
    print(teb)
    # plot_top_economical_bowlers(dict(teb))
    return dict(teb)


def extra_runs_per_team():
    con=connection()
    cur = con.cursor()
    cur.execute('select del.bowling_team,sum(del.extra_runs) from deliveries del inner join matches mat on del.match_id=mat.id where mat.season=2016 group by del.bowling_team;')
    exrpt = cur.fetchall()
    con.close()
    return dict(exrpt)
