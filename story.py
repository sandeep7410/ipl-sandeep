# probability of a team winning the match if it wins the toss and choose to bat in every year
import csv
import matplotlib.pyplot as plt
import utils
import sql_queries

def calculate(matches_path):
    seasons = {}
    with open(matches_path) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for match in matches_reader:
            if match['season'] not in seasons:
                seasons[match['season']] = {
                    'win': 0,
                    'tie': 0,
                    'lose': 0
                }
            if match['toss_decision'] == 'bat' and match['result'] != 'no result':
                if match['winner'] == match['toss_winner']:
                    seasons[match['season']]['win'] += 1
                elif match['result'] == 'tie':
                    seasons[match['season']]['tie'] += 1
                else:
                    seasons[match['season']]['lose'] += 1
    print(seasons)
    return seasons


def plot(seasons):
    i = 1
    j = 4
    for year in seasons:

        slices = [seasons[year]['win'], seasons[year]
                  ['lose'], seasons[year]['tie']]
        labels = ['Win', 'Lose', 'Tie']
        colors = ['#ff9999', '#66b3ff', '#99ff99']
        explosion = (0.1, 0.2, 0)
        # fig1, ax1 = plt.subplots()
        ax1 = plt.subplot2grid((2, 5), (i, j))
        ax1.pie(slices, explode=explosion, shadow=True,colors=colors, labels=labels,
                autopct='%1.1f%%', startangle=90,radius=2000)
        centre_circle = plt.Circle((0, 0), 0.70, fc='white')
        fig = plt.gcf()
        fig.gca().add_artist(centre_circle)
        ax1.axis('equal')
        # plt.tight_layout()
        plt.xlabel(year)
        if j < 1:
            j = 4
            i = i-1
        else:
            j = j-1

    plt.suptitle(
        'Probability of a team winning the match if it won the toss and choose to bat', fontsize=16,fontweight='bold')
    plt.show()


def calculate_and_plot(matches_path):
    result = calculate(matches_path)
    plot(result)

def sql_query():
    result=sql_queries.story()
    plot(result)
if __name__ == '__main__':
    calculate_and_plot('./matches.csv')
    sql_query()
