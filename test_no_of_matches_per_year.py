# no of matches per year
import unittest

from no_of_matches_per_year import *


class TestQuery1(unittest.TestCase):

    def test_calculate_no_of_matches_per_year(self):
        inputs_and_outputs = [
            ('./mock_matches.csv',  {'2017': 59, '2016': 60} )
        ]
        for inp, expected_output in inputs_and_outputs:
            output = calculate_no_of_matches_per_year(inp)
            self.assertEqual(output, expected_output)
        outputs = [{2016: 60,2017: 59}]
        for expected_output in outputs:
            output=sql_query()
            self.assertEqual(expected_output,output)

if __name__ == '__main__':
    unittest.main()