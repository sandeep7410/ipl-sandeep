# For the year 2016 plot the extra runs conceded per team.
import csv
import matplotlib.pyplot as plt
import utils
import sql_queries

def plot_extra_runs_per_team(extra_runs_per_team):

    plt.bar(extra_runs_per_team.keys(),
            extra_runs_per_team.values(), width=0.6)
    plt.title('Extra runs conceded per team in 2016')
    plt.xticks(rotation=15,fontsize=10)
    plt.show()


def calculate_extra_runs_per_team(deliveries_file_path,matches_file_path):
    extra_runs_per_team = {}
    queryYear = '2016'
    match_id_list = utils.get_match_id(matches_file_path,queryYear)
    with open(deliveries_file_path) as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        for ball in deliveries_reader:
            if ball['match_id'] not in match_id_list:
                continue
            else:
                if ball['bowling_team'] in extra_runs_per_team:
                    extra_runs_per_team[ball['bowling_team']
                                        ] += int(ball['extra_runs'])
                else:
                    extra_runs_per_team[ball['bowling_team']] = int(
                        ball['extra_runs'])
    print(extra_runs_per_team)
    return extra_runs_per_team


def calculate_and_plot_extra_runs_per_team(deliveries_file_path,matches_file_path):
    result = calculate_extra_runs_per_team(deliveries_file_path,matches_file_path)
    plot_extra_runs_per_team(result)

def sql_query():
    result=sql_queries.extra_runs_per_team()
    plot_extra_runs_per_team(result)


if __name__ == '__main__':
    calculate_and_plot_extra_runs_per_team('./deliveries.csv','./matches.csv')
    sql_query()
